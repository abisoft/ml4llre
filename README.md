Code for the paper Meta-Learning Improves Lifelong Relation Extraction.

It is based on Hong's repository: https://github.com/hongwang600/Lifelong_Relation_Detection

Tested on Python 3.6 and pytorch 0.4.1

How to run: ``python continue_train.py {fewrel,simplequestions} {100,200,300,..}``


