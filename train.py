import sys
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# import quadprog
import random
import time

from copy import deepcopy
from tqdm import tqdm

from data import gen_data, read_origin_relation
from model import SimilarityModel
from utils import process_testing_samples, process_samples, ranking_sequence,\
    copy_grad_data, get_grad_params
from evaluate import evaluate_model
from meta_loader import DataLoader as MetaDataLoader

from config import CONFIG_FEWREL,CONFIG_SIMPLEQUESTIONS

dataset = str(sys.argv[1])
if not (dataset in {"fewrel","simplequestions"}):
    print("ERROR: Specify dataset, one of {fewrel,simplequestions} ")
    sys.exit(0)
conf = CONFIG_FEWREL if (dataset == "fewrel") else CONFIG_SIMPLEQUESTIONS

embedding_dim = conf['embedding_dim']
hidden_dim = conf['hidden_dim']
batch_size = conf['batch_size']
model_path = conf['model_path']
device = conf['device']
lr = conf['learning_rate']
loss_margin = conf['loss_margin']

# META PARAMETERS
inner_batch_size = conf['inner_batch_size']
inner_iters = conf['inner_iters']
meta_step_size = conf['meta_step_size']
meta_step_size_final = conf['meta_step_size_final']
meta_batch_size = conf['meta_batch_size']
meta_iters = conf['meta_iters']
eval_inner_iters = conf['eval_inner_iters']
eval_interval = conf['eval_interval']
weight_decay_rate = conf['weight_decay_rate']


random.seed(100)
origin_relation_names = read_origin_relation()


def print_list(result):
    for num in result:
        sys.stdout.write('%.3f, ' %num)
    print('')

def feed_samples(model, samples, loss_function, all_relations, device,
                 alignment_model=None):
    questions, relations, relation_set_lengths = process_samples(
        samples, all_relations, device)
    ranked_questions, alignment_question_indexs = \
        ranking_sequence(questions)
    ranked_relations, alignment_relation_indexs =\
        ranking_sequence(relations)
    question_lengths = [len(question) for question in ranked_questions]
    relation_lengths = [len(relation) for relation in ranked_relations]
    pad_questions = torch.nn.utils.rnn.pad_sequence(ranked_questions)
    pad_relations = torch.nn.utils.rnn.pad_sequence(ranked_relations)
    pad_questions = pad_questions.to(device)
    pad_relations = pad_relations.to(device)

    model.zero_grad()
    if alignment_model is not None:
        alignment_model.zero_grad()
    model.init_hidden(device, sum(relation_set_lengths))
    all_scores = model(pad_questions, pad_relations, device,
                       alignment_question_indexs, alignment_relation_indexs,
                       question_lengths, relation_lengths, alignment_model)
    all_scores = all_scores.to('cpu')
    pos_scores = []
    neg_scores = []
    pos_index = []
    start_index = 0
    for length in relation_set_lengths:
        pos_index.append(start_index)
        pos_scores.append(all_scores[start_index].expand(length-1))
        neg_scores.append(all_scores[start_index+1:start_index+length])
        start_index += length
    pos_scores = torch.cat(pos_scores)
    neg_scores = torch.cat(neg_scores)

    loss = loss_function(pos_scores, neg_scores,
                         torch.ones(sum(relation_set_lengths)-
                                    len(relation_set_lengths)))
    loss.backward()
    return all_scores.detach(), loss.detach()

def train(training_data, valid_data, vocabulary, embedding_dim, hidden_dim,
          device, batch_size, lr, embedding, all_relations,
          model=None, metamodel=None, epoch=100, memory_data=[], loss_margin=0.5,
          alignment_model=None):

    if model is None:
        torch.manual_seed(100)
        model = SimilarityModel(embedding_dim, hidden_dim, len(vocabulary),
                                np.array(embedding), 1, device)

    loss_function = nn.MarginRankingLoss(loss_margin)
    if metamodel:
        model.load_state_dict(metamodel)
    model = model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=lr)
    best_acc = 0
    memory_index = 0

    validation_data = valid_data

    for m_iter in tqdm(range(1, meta_iters + 1), unit="Meta-Iters"):
        if len(memory_data) > 0:
            memory_batch = memory_data[memory_index]
            mtraining_data = [sample for sample in memory_batch] + training_data
            memory_index = (memory_index + 1) % len(memory_data)
        else:
            mtraining_data = training_data

        meta_training_data = MetaDataLoader(mtraining_data, batch_size)

        frac_done = (m_iter - 1) / meta_iters
        cur_meta_step_size = frac_done * meta_step_size_final + (1 - frac_done) * meta_step_size

        weights_original = deepcopy(model.state_dict())
        new_weights = []
        train_loss = []

        for mepoch in range(1, meta_batch_size + 1):
            epoch_loss = []
            start_time = time.time()

            for i in range(meta_batch_size):
                for batch in meta_training_data.sample_relation_instances(num_batches=inner_iters, rand=True):
                    scores, loss = feed_samples(model,batch,loss_function,all_relations, device,alignment_model)
                    optimizer.step()

            epoch_loss += [loss.item()]
            del scores, loss
            if epoch_loss:
                train_loss.extend(epoch_loss)
            new_weights.append(deepcopy(model.state_dict()))
            model.load_state_dict({name: weights_original[name] for name in weights_original})

        train_loss = np.sum(train_loss)  #
        results = [evaluate_model(model, validation_data, batch_size, all_relations, device, alignment_model)]
        print_list(results)
        print("meta iter {}: Total train_loss = {:.6f}".format(m_iter, train_loss))

        ws = len(new_weights)
        fweights = {name: new_weights[0][name] / float(ws) for name in new_weights[0]}
        for i in range(1, ws):
            for name in new_weights[i]:
                fweights[name] += new_weights[i][name] / float(ws)

        metamodel = {name: weights_original[name] + ((fweights[name] - weights_original[name]) * cur_meta_step_size)
                     for name in weights_original}
        model.load_state_dict(metamodel)

    print("Meta-training ended with {} meta iterations.".format(m_iter))
    return model, metamodel

if __name__ == '__main__':
    training_data, testing_data, valid_data, all_relations, vocabulary, \
        embedding=gen_data()
    train(training_data, valid_data, vocabulary, embedding_dim, hidden_dim,
          device, batch_size, lr, model_path, embedding, all_relations,
          model=None, epoch=100)
