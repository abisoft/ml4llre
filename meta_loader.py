"""
Data loader for TACRED json files.
"""

import json
import random
import torch
import numpy as np
from collections import defaultdict,OrderedDict

class DataLoader(object):
    """
    ########
    """
    def __init__(self, data_list, batch_size):
        self.batch_size = batch_size
        data = self.preprocess(data_list)
        total_batches = 0
        # chunk into batches
        for relation_name, instances in data.items():
            relation_data = data[relation_name]
            relation_data = [relation_data[i:i+batch_size] for i in range(0, len(relation_data), batch_size)]
            data[relation_name] = relation_data
            total_batches += len(relation_data)
        self.data = data
        self.relations = list(self.data.keys()) # this are their integer ids for now
        print("{} batches created".format(total_batches))

    def preprocess(self, data):
        """ Preprocess the data and convert to ids. """
        processed = OrderedDict()
        for line_parts in data:
            label_id = line_parts[0]
            label_ids_negatives = line_parts[1]
            tokens = line_parts[2]

            if label_id in processed:
                processed[label_id] += [(label_id, label_ids_negatives, tokens)]
            else:
                processed[label_id] = []
                processed[label_id] += [(label_id, label_ids_negatives, tokens)]
        return processed

    def gold(self):
        """ Return gold labels as a list. """
        return self.labels

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, key,relation_name):
        """ Get a batch with index. """
        if not isinstance(key, int):
            raise TypeError
        relation_instances = self.data[relation_name]
        if key < 0 or key >= len(relation_instances):
            raise IndexError
        batch = relation_instances[key]
        return batch

    def __iter__(self):
        for relation_name,batch_instances in self.data.items():
            for index,instance in enumerate(batch_instances):
                yield self.__getitem__(index,relation_name)

    def sample_relation_instances(self, relation_name=None, num_batches=-1,rand=False):
        if not relation_name:
            relation_name = random.choice(self.relations)
        data = self.data[relation_name]

        if (num_batches < 1) or (num_batches > len(data)):
            num_batches = len(data)

        indices = range(num_batches)
        if rand:
            indices = list(range(len(data)))
            np.random.shuffle(indices)
            indices = indices[:num_batches]

        for index in indices:
            yield self.__getitem__(index, relation_name)

